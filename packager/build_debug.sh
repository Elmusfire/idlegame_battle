cd ./lovejs/debug
python2 ../emscripten/tools/file_packager.py game.data --preload ../../../love@/ --js-output=game.js

cd ../release-performance/
python2 ../emscripten/tools/file_packager.py game.data --preload ../../../love@/ --js-output=game.js

cd ../../../

cp -r packager/lovejs/release-performance/ ./web
