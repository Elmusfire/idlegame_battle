
var Module;

if (typeof Module === 'undefined') Module = eval('(function() { try { return Module || {} } catch(e) { return {} } })()');

if (!Module.expectedDataFileDownloads) {
  Module.expectedDataFileDownloads = 0;
  Module.finishedDataFileDownloads = 0;
}
Module.expectedDataFileDownloads++;
(function() {
 var loadPackage = function(metadata) {

    var PACKAGE_PATH;
    if (typeof window === 'object') {
      PACKAGE_PATH = window['encodeURIComponent'](window.location.pathname.toString().substring(0, window.location.pathname.toString().lastIndexOf('/')) + '/');
    } else if (typeof location !== 'undefined') {
      // worker
      PACKAGE_PATH = encodeURIComponent(location.pathname.toString().substring(0, location.pathname.toString().lastIndexOf('/')) + '/');
    } else {
      throw 'using preloaded data can only be done on a web page or in a web worker';
    }
    var PACKAGE_NAME = 'game.data';
    var REMOTE_PACKAGE_BASE = 'game.data';
    if (typeof Module['locateFilePackage'] === 'function' && !Module['locateFile']) {
      Module['locateFile'] = Module['locateFilePackage'];
      Module.printErr('warning: you defined Module.locateFilePackage, that has been renamed to Module.locateFile (using your locateFilePackage for now)');
    }
    var REMOTE_PACKAGE_NAME = typeof Module['locateFile'] === 'function' ?
                              Module['locateFile'](REMOTE_PACKAGE_BASE) :
                              ((Module['filePackagePrefixURL'] || '') + REMOTE_PACKAGE_BASE);
  
    var REMOTE_PACKAGE_SIZE = metadata.remote_package_size;
    var PACKAGE_UUID = metadata.package_uuid;
  
    function fetchRemotePackage(packageName, packageSize, callback, errback) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', packageName, true);
      xhr.responseType = 'arraybuffer';
      xhr.onprogress = function(event) {
        var url = packageName;
        var size = packageSize;
        if (event.total) size = event.total;
        if (event.loaded) {
          if (!xhr.addedTotal) {
            xhr.addedTotal = true;
            if (!Module.dataFileDownloads) Module.dataFileDownloads = {};
            Module.dataFileDownloads[url] = {
              loaded: event.loaded,
              total: size
            };
          } else {
            Module.dataFileDownloads[url].loaded = event.loaded;
          }
          var total = 0;
          var loaded = 0;
          var num = 0;
          for (var download in Module.dataFileDownloads) {
          var data = Module.dataFileDownloads[download];
            total += data.total;
            loaded += data.loaded;
            num++;
          }
          total = Math.ceil(total * Module.expectedDataFileDownloads/num);
          if (Module['setStatus']) Module['setStatus']('Downloading data... (' + loaded + '/' + total + ')');
        } else if (!Module.dataFileDownloads) {
          if (Module['setStatus']) Module['setStatus']('Downloading data...');
        }
      };
      xhr.onload = function(event) {
        var packageData = xhr.response;
        callback(packageData);
      };
      xhr.send(null);
    };

    function handleError(error) {
      console.error('package error:', error);
    };
  
      var fetched = null, fetchedCallback = null;
      fetchRemotePackage(REMOTE_PACKAGE_NAME, REMOTE_PACKAGE_SIZE, function(data) {
        if (fetchedCallback) {
          fetchedCallback(data);
          fetchedCallback = null;
        } else {
          fetched = data;
        }
      }, handleError);
    
  function runWithFS() {

    function assert(check, msg) {
      if (!check) throw msg + new Error().stack;
    }
Module['FS_createPath']('/', 'monsters', true, true);

    function DataRequest(start, end, crunched, audio) {
      this.start = start;
      this.end = end;
      this.crunched = crunched;
      this.audio = audio;
    }
    DataRequest.prototype = {
      requests: {},
      open: function(mode, name) {
        this.name = name;
        this.requests[name] = this;
        Module['addRunDependency']('fp ' + this.name);
      },
      send: function() {},
      onload: function() {
        var byteArray = this.byteArray.subarray(this.start, this.end);

          this.finish(byteArray);

      },
      finish: function(byteArray) {
        var that = this;

        Module['FS_createDataFile'](this.name, null, byteArray, true, true, true); // canOwn this data in the filesystem, it is a slide into the heap that will never change
        Module['removeRunDependency']('fp ' + that.name);

        this.requests[this.name] = null;
      },
    };

        var files = metadata.files;
        for (i = 0; i < files.length; ++i) {
          new DataRequest(files[i].start, files[i].end, files[i].crunched, files[i].audio).open('GET', files[i].filename);
        }

  
    function processPackageData(arrayBuffer) {
      Module.finishedDataFileDownloads++;
      assert(arrayBuffer, 'Loading data file failed.');
      assert(arrayBuffer instanceof ArrayBuffer, 'bad input to processPackageData');
      var byteArray = new Uint8Array(arrayBuffer);
      var curr;
      
        // copy the entire loaded file into a spot in the heap. Files will refer to slices in that. They cannot be freed though
        // (we may be allocating before malloc is ready, during startup).
        if (Module['SPLIT_MEMORY']) Module.printErr('warning: you should run the file packager with --no-heap-copy when SPLIT_MEMORY is used, otherwise copying into the heap may fail due to the splitting');
        var ptr = Module['getMemory'](byteArray.length);
        Module['HEAPU8'].set(byteArray, ptr);
        DataRequest.prototype.byteArray = Module['HEAPU8'].subarray(ptr, ptr+byteArray.length);
  
          var files = metadata.files;
          for (i = 0; i < files.length; ++i) {
            DataRequest.prototype.requests[files[i].filename].onload();
          }
              Module['removeRunDependency']('datafile_game.data');

    };
    Module['addRunDependency']('datafile_game.data');
  
    if (!Module.preloadResults) Module.preloadResults = {};
  
      Module.preloadResults[PACKAGE_NAME] = {fromCache: false};
      if (fetched) {
        processPackageData(fetched);
        fetched = null;
      } else {
        fetchedCallback = processPackageData;
      }
    
  }
  if (Module['calledRun']) {
    runWithFS();
  } else {
    if (!Module['preRun']) Module['preRun'] = [];
    Module["preRun"].push(runWithFS); // FS is not initialized yet, wait for it
  }

 }
 loadPackage({"files": [{"audio": 0, "start": 0, "crunched": 0, "end": 990, "filename": "/monsterlist.txt"}, {"audio": 0, "start": 990, "crunched": 0, "end": 11232, "filename": "/main.lua"}, {"audio": 0, "start": 11232, "crunched": 0, "end": 11622, "filename": "/monsters/water_nymph.png"}, {"audio": 0, "start": 11622, "crunched": 0, "end": 12117, "filename": "/monsters/giant_beetle.png"}, {"audio": 0, "start": 12117, "crunched": 0, "end": 13754, "filename": "/monsters/zonguldrok_lich_1.png"}, {"audio": 0, "start": 13754, "crunched": 0, "end": 14581, "filename": "/monsters/death_cob.png"}, {"audio": 0, "start": 14581, "crunched": 0, "end": 15097, "filename": "/monsters/merfolk_aquamancer_water_new.png"}, {"audio": 0, "start": 15097, "crunched": 0, "end": 15602, "filename": "/monsters/ice_dragon_new.png"}, {"audio": 0, "start": 15602, "crunched": 0, "end": 16106, "filename": "/monsters/jelly.png"}, {"audio": 0, "start": 16106, "crunched": 0, "end": 16790, "filename": "/monsters/unseen_horror_new.png"}, {"audio": 0, "start": 16790, "crunched": 0, "end": 17730, "filename": "/monsters/giant_blowfly.png"}, {"audio": 0, "start": 17730, "crunched": 0, "end": 18681, "filename": "/monsters/draconic_base-purple_new.png"}, {"audio": 0, "start": 18681, "crunched": 0, "end": 19159, "filename": "/monsters/reaper_new.png"}, {"audio": 0, "start": 19159, "crunched": 0, "end": 20786, "filename": "/monsters/quicksilver_dragon_new.png"}, {"audio": 0, "start": 20786, "crunched": 0, "end": 22255, "filename": "/monsters/ghoul.png"}, {"audio": 0, "start": 22255, "crunched": 0, "end": 23313, "filename": "/monsters/shadow_new.png"}, {"audio": 0, "start": 23313, "crunched": 0, "end": 23766, "filename": "/monsters/shark_new.png"}, {"audio": 0, "start": 23766, "crunched": 0, "end": 24426, "filename": "/monsters/rotting_devil.png"}, {"audio": 0, "start": 24426, "crunched": 0, "end": 24944, "filename": "/monsters/profane_servitor.png"}, {"audio": 0, "start": 24944, "crunched": 0, "end": 25331, "filename": "/monsters/black_mamba_new.png"}, {"audio": 0, "start": 25331, "crunched": 0, "end": 25862, "filename": "/monsters/golden_dragon.png"}, {"audio": 0, "start": 25862, "crunched": 0, "end": 27053, "filename": "/monsters/harpy.png"}, {"audio": 0, "start": 27053, "crunched": 0, "end": 27567, "filename": "/monsters/silent_spectre.png"}, {"audio": 0, "start": 27567, "crunched": 0, "end": 29311, "filename": "/monsters/kobold_demonologist.png"}, {"audio": 0, "start": 29311, "crunched": 0, "end": 30381, "filename": "/monsters/flayed_ghost_new.png"}, {"audio": 0, "start": 30381, "crunched": 0, "end": 31008, "filename": "/monsters/orc_priest_new.png"}, {"audio": 0, "start": 31008, "crunched": 0, "end": 31990, "filename": "/monsters/guardian_naga.png"}, {"audio": 0, "start": 31990, "crunched": 0, "end": 33925, "filename": "/monsters/griffon.png"}, {"audio": 0, "start": 33925, "crunched": 0, "end": 34419, "filename": "/monsters/shadow_demon.png"}, {"audio": 0, "start": 34419, "crunched": 0, "end": 35246, "filename": "/monsters/boring_beetle.png"}, {"audio": 0, "start": 35246, "crunched": 0, "end": 35663, "filename": "/monsters/basilisk.png"}, {"audio": 0, "start": 35663, "crunched": 0, "end": 36315, "filename": "/monsters/grizzly_bear.png"}, {"audio": 0, "start": 36315, "crunched": 0, "end": 37071, "filename": "/monsters/iron_devil.png"}, {"audio": 0, "start": 37071, "crunched": 0, "end": 37816, "filename": "/monsters/boulder_beetle.png"}, {"audio": 0, "start": 37816, "crunched": 0, "end": 38288, "filename": "/monsters/wight_new.png"}, {"audio": 0, "start": 38288, "crunched": 0, "end": 39434, "filename": "/monsters/drowned_soul.png"}, {"audio": 0, "start": 39434, "crunched": 0, "end": 39955, "filename": "/monsters/juggernaut.png"}, {"audio": 0, "start": 39955, "crunched": 0, "end": 40836, "filename": "/monsters/iguana.png"}, {"audio": 0, "start": 40836, "crunched": 0, "end": 41767, "filename": "/monsters/butterfly_4_new.png"}, {"audio": 0, "start": 41767, "crunched": 0, "end": 44058, "filename": "/monsters/elephant_dire_old.png"}, {"audio": 0, "start": 44058, "crunched": 0, "end": 44459, "filename": "/monsters/eidolon.png"}, {"audio": 0, "start": 44459, "crunched": 0, "end": 44988, "filename": "/monsters/ufetubus.png"}, {"audio": 0, "start": 44988, "crunched": 0, "end": 45499, "filename": "/monsters/ghost_moth_new.png"}, {"audio": 0, "start": 45499, "crunched": 0, "end": 46312, "filename": "/monsters/hippogriff_new.png"}, {"audio": 0, "start": 46312, "crunched": 0, "end": 46747, "filename": "/monsters/bat.png"}, {"audio": 0, "start": 46747, "crunched": 0, "end": 47186, "filename": "/monsters/skeletal_warrior_new.png"}, {"audio": 0, "start": 47186, "crunched": 0, "end": 47822, "filename": "/monsters/wyvern_new.png"}, {"audio": 0, "start": 47822, "crunched": 0, "end": 48197, "filename": "/monsters/forest_drake.png"}, {"audio": 0, "start": 48197, "crunched": 0, "end": 48776, "filename": "/monsters/human_monk_ghost.png"}, {"audio": 0, "start": 48776, "crunched": 0, "end": 49200, "filename": "/monsters/orange_demon_new.png"}, {"audio": 0, "start": 49200, "crunched": 0, "end": 49618, "filename": "/monsters/unborn.png"}, {"audio": 0, "start": 49618, "crunched": 0, "end": 50600, "filename": "/monsters/draconic_base-red_new.png"}, {"audio": 0, "start": 50600, "crunched": 0, "end": 51365, "filename": "/monsters/guardian_serpent_new.png"}, {"audio": 0, "start": 51365, "crunched": 0, "end": 52833, "filename": "/monsters/kenku_winged.png"}, {"audio": 0, "start": 52833, "crunched": 0, "end": 53347, "filename": "/monsters/wight_king.png"}, {"audio": 0, "start": 53347, "crunched": 0, "end": 53922, "filename": "/monsters/hungry_ghost.png"}, {"audio": 0, "start": 53922, "crunched": 0, "end": 54585, "filename": "/monsters/mermaid_water.png"}, {"audio": 0, "start": 54585, "crunched": 0, "end": 55257, "filename": "/monsters/storm_dragon_new.png"}, {"audio": 0, "start": 55257, "crunched": 0, "end": 56114, "filename": "/monsters/draconic_base-green_new.png"}, {"audio": 0, "start": 56114, "crunched": 0, "end": 56628, "filename": "/monsters/hill_giant_new.png"}, {"audio": 0, "start": 56628, "crunched": 0, "end": 57293, "filename": "/monsters/ugly_thing.png"}, {"audio": 0, "start": 57293, "crunched": 0, "end": 58657, "filename": "/monsters/ice_beast.png"}, {"audio": 0, "start": 58657, "crunched": 0, "end": 59707, "filename": "/monsters/fiend.png"}], "remote_package_size": 59707, "package_uuid": "72d00380-d4d3-40df-96e2-d3c074379762"});

})();
