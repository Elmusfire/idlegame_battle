allspecies = {}

playerteam = {}
enemyteam = {}

attacktimer = 0

speedmod = 1
shatter = 0.1
power_curve = 1.5
cost_curve = 1.1
hits_curve = 1.1
default_hits = 3
start_value = 100^2
regen_mod = 0.25
spread = 0
permadeath = false
enemycostfactor = 2
startprice = 100
team_size = 4


instant_resolve = false

chain_bonus = 0


money = 100
level = 0
shopsize = 8


-- get all lines from a file, returns an empty
-- list/table if the file does not exist
function lines_from(file)
  str,size = love.filesystem.read(file)
  lines = {}
  for s in str:gmatch("[^\r\n]+") do
      table.insert(lines, s)
  end
  return lines
end

function love.load()
    Q = lines_from("monsterlist.txt")
    for key,value in pairs(Q) do
        create_species(value,1)
    end
    for i=1,team_size do
        new_monster(playerteam,i+1)
    end
    new_monster(enemyteam,1)
    print(allspecies)
end


function create_species(filename,ratio)
    local monsterdata = {}
    monsterdata.image = love.graphics.newImage( "monsters/" .. filename )

    ratio = ratio ^ spread
    hits_to_kill = ratio*default_hits * hits_curve^(#allspecies)

    value = power_curve^(#allspecies) * start_value
    cost = cost_curve^(#allspecies) * value
    print(value ..","..#allspecies)
    monsterdata.power = math.sqrt(value/hits_to_kill)
    monsterdata.health = math.sqrt(value*hits_to_kill)
    monsterdata.cost = cost*startprice/start_value
    monsterdata.drop = value * enemycostfactor*startprice/start_value

    table.insert(allspecies,monsterdata)
end

function new_monster(parent,index,swap)
    for i=1,team_size do
        if parent[i] == nil then
            newmonster = {}
            parent[i] = newmonster
            newmonster.index = index
            newmonster.epic = 0
            if allspecies[index] ~= nil then
                newmonster.health = allspecies[index].health
                newmonster.maxhealth = allspecies[index].health
                newmonster.power = allspecies[index].power
            end
            newmonster.kills = 0
            if swap == true then
                activate(parent,i)
            end
            return true
        end
    end
    return false
end


function activate(parent,index)

    tmp = parent.active
    parent.active = parent[index]
    parent[index] = tmp
end

function isdead(object)
    return (object == nil or object.health <= 0)
end

function heal(index,costmod,relamount)
    if playerteam[index] == nil then
        return
    else
        ha = math.min(playerteam[index].maxhealth-playerteam[index].health,relamount*playerteam[index].maxhealth)
        hc = ha/playerteam[index].maxhealth*allspecies[playerteam[index].index].cost*costmod
        if money > hc or hc == 0 then
            money = money - hc
            playerteam[index].health = math.min(playerteam[index].health+ha,playerteam[index].maxhealth)
        end
    end

end

function regen(parent)
    for i=1,team_size do
        if parent[i] ~= nil then
            heal(i,0,1.0/default_hits*speedmod*shatter*regen_mod)
            if instant_resolve then
                heal(i,0,1)
            end
        end
    end
end

function replace_death(parent)
    for i=1,team_size do
        if isdead(parent.active) then
            rotate(parent)
        end
    end
end

function fill_enemy_team()
    for i=1,team_size do
        if isdead(enemyteam[i]) then
            level = top_buy()
            index = math.random(math.max(1,math.floor((level-6)*0.9)),math.max(level-1,2))

            new_monster(enemyteam,index)
        end
    end
end



function sell(index)
    if playerteam[index] == nil then
        return
    else
        money = money + allspecies[playerteam[index].index].cost
        playerteam[index] = nil
    end
end

function sell_worst()
    w = 0
    for i=1,team_size do
        if w==0 or w>playerteam[i].index then
            w = playerteam[i].index
        end
    end
    for i=1,team_size do
        if w==playerteam[i].index then
            sell(i)
            return
        end
    end
end

function buy(index)
    index = top_buy_2() + index - shopsize

    if money > allspecies[index].cost then

        if not new_monster(playerteam,index,true) then
            sell_worst()
            new_monster(playerteam,index,false)
        end
        money = money - allspecies[index].cost

    end


end

function top_buy()
    for i=1,#allspecies do
        if allspecies[i].cost > money then
            return i-1
        end
    end
    return #allspecies
end

function top_buy_2()
    return math.max(top_buy(),1+shopsize)
end

function rotate(parent)
    for i=team_size,1,-1 do
        if parent[i] ~= nil then
            activate(parent,i)
        end
    end
end

function attack()
    p1 = playerteam.active
    p2 = enemyteam.active
    if p1 == nil or p2 == nil then
        replace_death(playerteam)
        replace_death(enemyteam)
        return
    end

    dam1 =  p1.power*speedmod*shatter
    dam2 = p2.power*speedmod*shatter

    if (p1.health < dam2 and p2.health < dam1) or instant_resolve then
        part = math.max(dam1/p2.health,dam2/p1.health)
        dam1 = dam1 / part
        dam2 = dam2 / part
    end

    p1.health = math.max(p1.health - dam2,0)
    p2.health = math.max(p2.health - dam1,0)

    if p2.health <= 0 then
        chain_bonus = chain_bonus + 1
        money = money + allspecies[p2.index].drop * chain_bonus
        enemyteam.active = nil
        playerteam.active.kills = playerteam.active.kills + 1
    end

    if p1.health <= 0 then
        chain_bonus = 0
        if enemyteam.active ~= nil then
            enemyteam.active.kills = enemyteam.active.kills + 1
        end
        if permadeath then
            playerteam.active = nil
        end
    end
    fill_enemy_team()
    if not permadeath then
        regen(playerteam)
    end
    replace_death(playerteam)
    replace_death(enemyteam)
end


function love.update()
    local timeinc = shatter
    local dt  = love.timer.getDelta( )
    attacktimer=attacktimer+dt
    love.timer.sleep(timeinc - attacktimer)
    if attacktimer > timeinc then
        attack()
        attacktimer=attacktimer-timeinc
    end


end

function love.mousepressed( x, y, button, istouch, presses )
    print("click")
    if button == 1 then
        if x > 100 and x < 132 then
            index = math.floor(y/52)
            gait = y - index*52
            if index < team_size+1 and index > 0 then
                if gait < 32 then
                    activate(playerteam,index)
                    chain_bonus = 0
                else
                    sell(index)
                end
            end
        end
        if y > 400 and x > 100 then
            index = math.floor((x-60)/50)
            if index < shopsize+1 and index > 0 then
                buy(index)
            end
        end
    end
end


function draw_teams()
    local size = 52
    for i=1,team_size do
        draw_monster(playerteam[i],100,i*size,2)
        draw_monster(enemyteam[i],500,i*size,2)
    end
    draw_monster(playerteam.active,200,2*size,2,2)
    draw_monster(enemyteam.active,400,2*size,2,2)

end



function draw_monster(data,x,y,border,scale)
    setColor(0,0,0)
    if scale == nil then
        scale = 1
    end
    love.graphics.rectangle( "line", x-border*scale, y-border*scale, scale*(32+2*border),scale*(45+2*border ) )
    if data == nil then
        return
    end
    local speciesdata = allspecies[data.index]
    setColor(1,1,1)
    love.graphics.draw( speciesdata.image,x,y,0,scale,scale)
    local relhealth = data.health / speciesdata.health
    if relhealth > 0.5 then
        setColor(0,0.5,0)
    elseif relhealth > 0.2 then
        setColor(1,0.5,0)
    else
        setColor(1,0,0)
    end
    love.graphics.rectangle( "fill", x, y+32*scale, 32*relhealth*scale, 8*scale )
    setColor(1,1,1)
    --love.graphics.print(""..data.kills,x+32*scale,y)

end

function draw_species(index,x,y,border)
    setColor(0,0,0)
    love.graphics.rectangle( "line", x-border, y-border, 40+2*border,50+2*border  )
    local speciesdata = allspecies[index]
    if speciesdata == nil then
        return
    end
    setColor(1,1,1)
    love.graphics.draw( speciesdata.image,x,y)
    love.graphics.print(display_money(speciesdata.cost,"€"), x,y+36)
    love.graphics.print(display_money(speciesdata.health,""), x,y+55)
    love.graphics.print(display_money(speciesdata.power,""), x,y+68)
end



function display_money(amount,prefix)
    if amount == 0 then
        return prefix.."0"
    end
    V = 'kMGTPEZY'
    order = math.floor(math.log10(amount)/3)

    if order > 0 then
        suffix = V:sub(order,order)
    else
        suffix = ""
    end
    div = amount/(1000^order)

    if div < 10 then
        div = 0.1*math.floor(div*10)
    else
        div = math.floor(div)
    end
    return prefix..div..suffix
end

function draw_shop()
    for i=1,shopsize+1 do
        draw_species(top_buy_2()+i-shopsize,60+50*i,400,2)
    end
end

function get_active_status(parent)
    if parent.active ~= nil then
        return display_money(parent.active.health,"").."/"..display_money(parent.active.maxhealth,"").."\ndps:"..display_money(parent.active.power,"")
    end
    return ""
end

function setColor(r,g,b)
    if love._version_minor <= 10 then
        love.graphics.setColor(r*255,g*255,b*255)
    else
        love.graphics.setColor(r,g,b)
    end
end

function setBackgroundColor(r,g,b)
    if love._version_minor <= 10 then
        love.graphics.setBackgroundColor(r*255,g*255,b*255)
    else
        love.graphics.setBackgroundColor(r,g,b)
    end
end

function love.draw()
    setBackgroundColor( 0, 0.5, 1)
    setColor(0,0,0)
    love.graphics.print( "money: " .. display_money(money,"€") .. "\nlevel: "..level, 100, 10)
    setColor(1,0,0)
    love.graphics.print( chain_bonus, 300, 100,0,5,5)
    setColor(0,0,0)
    if enemyteam.active ~= nil then
        love.graphics.print( "money on \nnext kill:\n" .. display_money(allspecies[enemyteam.active.index].drop,"€").." x".. (chain_bonus + 1), 300, 160)
    end
    love.graphics.print( get_active_status(playerteam), 200, 200)
    love.graphics.print( get_active_status(enemyteam), 400, 200)
    draw_teams()
    draw_shop()

end
